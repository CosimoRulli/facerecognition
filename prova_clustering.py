from matplotlib import pyplot as plt
from scipy.cluster.hierarchy import dendrogram, linkage
import numpy as np
from scipy.cluster.hierarchy import fcluster
#from vgg import *
from vgg_alt import *
from utilities import *
from matplotlib.patches import Circle, Wedge, Polygon
import os
import cv2

def fancy_dendrogram(*args, **kwargs):
    max_d = kwargs.pop('max_d', None)
    if max_d and 'color_threshold' not in kwargs:
        kwargs['color_threshold'] = max_d
    annotate_above = kwargs.pop('annotate_above', 0)

    ddata = dendrogram(*args, **kwargs)

    if not kwargs.get('no_plot', False):
        plt.title('Hierarchical Clustering Dendrogram (truncated)')
        plt.xlabel('sample index or (cluster size)')
        plt.ylabel('distance')
        for i, d, c in zip(ddata['icoord'], ddata['dcoord'], ddata['color_list']):
            x = 0.5 * sum(i[1:3])
            y = d[1]
            if y > annotate_above:
                plt.plot(x, y, 'o', c=c)
                plt.annotate("%.3g" % y, (x, y), xytext=(0, -5),
                             textcoords='offset points',
                             va='top', ha='center')
        if max_d:
            plt.axhline(y=max_d, c='k')
    return ddata


def find_closest(points_set, center):
    center=np.array(center)
    min_dist=np.inf
    centroid=None
    for point in points_set:
        point=np.array(point)
        dist = np.linalg.norm(point - center)
        if(dist <min_dist):
            centroid=point
            min_dist=dist
    return centroid

if __name__=="__main__":
    features=[]
    #todo IGNORA
    #vgg=load_vgg()
    #cap = cv2.VideoCapture(0)
    # path = "/home/cosimo/Scrivania/ImmaginiVolto/"
    # for im in os.listdir(path):
    #     full_path= path + im
    #     img = cv2.imread(full_path)
    #     b_box=extract_faces(img)
    #     faces = []
    #     for (x, y, w, h) in b_box:
    #         faces.append(img[y:(y + h), x:(x + w)])
    #     face=faces[0]
    #     features.append(extract_features_alt(face))
    #
    # X=np.array(features)

    np.random.seed(4711)  # for repeatability of this tutorial
    a = np.random.multivariate_normal([10, 0], [[3, 1], [1, 4]], size=[100,])
    b = np.random.multivariate_normal([0, 20], [[3, 1], [1, 4]], size=[50,])
    X = np.concatenate((a, b), )
    # todo prima di assegnare devo trasformare in un array np di dimensione nxm, dove n è il numero di esempi e m le features

    #X = features
    #print (X.shape )# 150 samples with 2 dimensions
    #plt.show()


    #dentro Z abbiamo il vettore dei collegamenti. Z[0] indica i punti che sono stati fusi al passo 0
    Z = linkage(X, 'single', metric='cosine')
    # print(Z.shape)
    # plt.figure(figsize=(25, 10))
    # plt.title('Hierarchical Clustering Dendrogram')
    # plt.xlabel('sample index')
    # plt.ylabel('distance')
    # dendrogram(
    #     Z,
    #     leaf_rotation=90.,  # rotates the x axis labels
    #     leaf_font_size=8.,  # font size for the x axis labels
    # )
    #plt.show()


    #Scegliere in modo manuale il troncamento
    # max_d = 50  # max_d as in max_distance
    # fancy_dendrogram(
    #     Z,
    #     truncate_mode='lastp',
    #     p=12,
    #
    #     leaf_rotation=90.,
    #     leaf_font_size=12.,
    #     show_contracted=True,
    #     annotate_above=10,
    #     max_d=max_d,  # plot a horizontal cut-off line
    # )
    # plt.show()
    #ESTRARRE I CLUSTER

    #in base a max_D
    #clusters = fcluster(Z, max_d, criterion='distance')

    #in base al numero di cluster k
    k=2
    clusters_list= fcluster(Z,k, criterion='maxclust')

    #in base all'euristica inconistency method
    #clusterIM=fcluster(Z, 8, depth=10)

    print(clusters_list)

    clusters=np.empty(k, dtype=object)
    for i in range(0,k):
        clusters[i]=[]
    for i in range(clusters_list.shape[0]):
        index=clusters_list[i]-1
        #np.append(clusters[index],X[index]) #Aggiunge X[index] a cluster[index]
        clusters[index].append(X[i])
    print(clusters)

    centroids=[]
    centers=[]
    variances=[]
    for i in range(0,k):
        clusters[i]=np.array(clusters[i])
        clust_center= np.mean(clusters[i], axis=0)
        centers.append(clust_center)
        centroids.append(find_closest(clusters[i], clust_center))
        variances.append(np.var(clusters[i], axis=0))


    centroids=np.array(centroids)
    centers=np.array(centers)
    print(centers)
    print(centroids)
    fig = plt.figure(1)

    plt.scatter(X[:,0], X[:,1])
    ax = fig.add_subplot(1, 1, 1)



    # ax.plot(centers[:,0], centers[:,1], color="green")
    # ax.plot(centroids[:,0], centroids[:,1], "r-")
    radius= variances[0][1]
    center =centers[0]
    circ = plt.Circle(center, radius=radius, color='g', fill=False)
    #plt.axis('scaled')
    ax.add_patch(circ)
    #ax.plot(Circle(centers[0,0], variances[0]), color="blue")
    plt.show()