
from vgg import extract_features
from scipy.spatial.distance import cosine
from sklearn.cluster import AgglomerativeClustering
from matplotlib import pyplot as plt
from scipy.cluster.hierarchy import dendrogram, linkage
import numpy as np
from scipy.cluster.hierarchy import fcluster

n=10 #numero di descrittori dopo il clustering



'''
Idea per un nuovo algoritmo

raccolta di 2.5*n descrittori
calcolo del centro dei descrittori
calcolo della massima distanza componente per componente tra centro e i descrittori (otteniamo un vettore RAY con tante dimensioni quante ne hanno i descrittori)
clustering
aggiunta dei descrittori fino a 2.5 n
ripeti il procedimento di aggiornamento di RAY e di clustering 


'''
class Subject:

    SUBJ_ID = 0

    def __init__(self):

        self.id = Subject.SUBJ_ID
        Subject.SUBJ_ID += 1
        self.n_clusters = n
        self.max_features = int(2.5*n)
        self.name=None
        self.surname=None
        self.descriptors=[]
        self.center = None
        self.variance = None
        self.dispersion_index=0
        self.stat=False #TODO DEBUG
        self.clusterized = False
        #manca un indice di dispersione, per ora troppo vago per poter essere definito come struttura

    def update_descriptor(self, face_descriptor ): #aggiunge il descrittore se c'è posto altrimenti clustering

        if(len(self.descriptors) < self.max_features):
            self.descriptors.append(face_descriptor)
        else:
            self.clustering_on_descriptors()

        self.update_center()
        # self.update_variance() --> todo commentato

    # def get_distance(self, face_descriptor):
    #     dist = cosine(face_descriptor, self.center)
    #     return dist


    def update_center(self):
        descriptors_array=np.array(self.descriptors)
        self.center=np.mean(descriptors_array, axis=0)

    def update_variance(self):
        descriptors_array=np.array(self.descriptors)
        self.variance=np.var(descriptors_array, axis=0)#la varianza è un vettore adesso


    def update_dispersion_metric(self):
        self.variance = np.inf
       # print('hello magi')


    def find_centroids(self, clusters_map, descriptors_array):
        clusters = np.empty(self.n_clusters, dtype=object)
        for i in range(0, self.n_clusters):
            clusters[i] = []
        for i in range(clusters_map.shape[0]):
            index = clusters_map[i] - 1
            # np.append(clusters[index],X[index]) #Aggiunge X[index] a cluster[index]
            clusters[index].append(descriptors_array[i])
        centroids = []
        for i in range(0, self.n_clusters):
            clusters[i] = np.array(clusters[i])
            clust_center = np.mean(clusters[i], axis=0)
            #todo attenzione, find closest è implementato con la distanza euclidea
            centroids.append(self.find_closest(clusters[i], clust_center))
        return centroids

    def find_closest(self,points_set, center):
        # todo attenzione, find closest è implementato con la distanza euclidea
        center = np.array(center)
        min_dist = np.inf
        centroid = None
        for point in points_set:
            point = np.array(point)
            dist = np.linalg.norm(point - center)
            if (dist < min_dist):
                centroid = point
                min_dist = dist
        return centroid

    def clustering_on_descriptors(self):
        # Affinity = {“euclidean”, “l1”, “l2”, “manhattan”,
        # “cosine”}
        # Linkage = {“ward”, “complete”, “average”}
        # Hclustering = AgglomerativeClustering(n_clusters=10,
        #                                       affinity='cosine', linkage ='ward')
        # Hclustering.fit(self.descriptors)

        #fixme converebbe mantenere direttamente un np.array
        descriptors_array=np.array(self.descriptors)
        Z = linkage(descriptors_array, 'single', metric='cosine') #https://docs.scipy.org/doc/scipy/reference/generated/scipy.cluster.hierarchy.linkage.html
        clusters_map = fcluster(Z, self.n_clusters, criterion='maxclust')#--> vettore lungo quanto il numero di descrittori, ha una label che indica a quale cluster appartiene ciascun descrittore
        centroids= self.find_centroids( clusters_map, descriptors_array)
        self.update_center()
        self.update_dispersion_index()
        self.descriptors=centroids
        self.clusterized = True




    def close_enough_th(self, descriptor):
       return cosine(descriptor, self.center) < 0.20

       #return (np.less_equal(descriptor, self.center+ 2*self.variance)).all()# varianza troppo piccola!!!
       #return (np.less_equal(descriptor, self.center + 1.5*self.dispersion_index).all() )


    def close_enough_di(self, descriptor):
        v_distances = []
        for i in range(len(self.descriptors)):
            for j in range(i + 1, len(self.descriptors)):
                # print(cosine(self.descriptors[i], self.descriptors[j]))
                v_distances.append(abs(self.descriptors[i]- self.descriptors[j]))


        #return np.less_equal(descriptor, self.center + self.dispersion_index +np.max(np.array(v_distances),axis=0)).all()

        if np.array(v_distances).any():
            cond = np.sum(np.less_equal(descriptor, self.center + self.dispersion_index +np.max(np.array(v_distances),axis=0))) > len(descriptor)*0.90
            return cond
        else:
            return False

    def close_enough_n(self, descriptor):
        distances = []
        for i in range(len(self.descriptors)):
            for j in range(i + 1, len(self.descriptors)):
                # print(cosine(self.descriptors[i], self.descriptors[j]))
                distances.append(cosine(self.descriptors[i], self.descriptors[j]))
        th=2*np.max(distances)
        dists = []
        for row in self.descriptors:
            dists.append(cosine(descriptor, row))
        sum= np.sum(np.less_equal(dists, th))

        return sum > (len(self.descriptors)*0.2)

    def close_enough_eucl_old(self, descriptor):
        distances = []  # distanze tra i descrittori
        if len(self.descriptors)==1:
            return False
        for i in range(len(self.descriptors)):
            for j in range(i + 1, len(self.descriptors)):
                # print(cosine(self.descriptors[i], self.descriptors[j]))
                distances.append(np.linalg.norm(self.descriptors[i] - self.descriptors[j]))
        mean_dist=np.mean(distances)
        max_dist=np.max(distances)
        min_dist=np.min(distances)
        th=max_dist
        #th= (min_dist + max_dist) / 2
        if( max_dist >2*mean_dist):
            th=(mean_dist+max_dist)/2

        var=np.var(distances)
        distances_new = []  # distanze tra i descrittori e il nuovo descrittore
        counter=0
        for my_descriptor in self.descriptors:
            #distances_new.append(np.linalg.norm(my_descriptor - descriptor))
            current_dist=np.linalg.norm(my_descriptor - descriptor)
            if current_dist < th:
                counter+=1
        return counter > len(self.descriptors)*0.3




    def close_enough_eucl(self, descriptor):
        distances = []  # distanze tra i descrittori
        if len(self.descriptors)==1:
            return False
        for i in range(len(self.descriptors)):
            for j in range(i + 1, len(self.descriptors)):
                # print(cosine(self.descriptors[i], self.descriptors[j]))
                distances.append(np.linalg.norm(self.descriptors[i] - self.descriptors[j]))
        mean_dist=np.mean(distances)
        max_dist=np.max(distances)
        min_dist=np.min(distances)
        th=2.5*np.log(1+min_dist)
        #th= (min_dist + max_dist) / 2
        # if( max_dist >2*mean_dist):
        #     th=(mean_dist+max_dist)/2

        var=np.var(distances)
        distances_new = []  # distanze tra i descrittori e il nuovo descrittore
        counter=0
        for my_descriptor in self.descriptors:
            #distances_new.append(np.linalg.norm(my_descriptor - descriptor))
            current_dist=np.linalg.norm(my_descriptor - descriptor)
            if current_dist < th:
                counter+=1
        return (counter,counter >=  1)



#https://cmusatyalab.github.io/openface/demo-2-comparison/

    def update_dispersion_index(self):
        # for i in range(len(self.descriptors[0])):
        #     max_dist=0
        #     for j in range(len(self.descriptors)):
        #         dist=np.linalg.norm(self.center-self.descriptors[j][i])
        #         if(dist< max_dist):
        #             max_dist=dist
        #
        #     self.dispersion_index[i]=max_dist

        max= 0
        for descriptor in self.descriptors:
            max=np.maximum(max, np.absolute(self.center-descriptor))


        self.dispersion_index=np.maximum(max, self.dispersion_index)
        self.stat=True



    def compute_hyperplane(self, known_subjects):
        negative_f=[]
        for subject in known_subjects:
            if(not (subject ==  self)):
                negative_f.append(self.descriptors)
        if negative_f:
            negative_f=np.array(negative_f)
            w= np.mean(self.descriptors)- np.mean(negative_f)

            min_p=np.min(np.dot(self.descriptors, w))
            max_n=np.max(np.dot(negative_f, w))
            b=0.5*min_p+ 0.5*max_n
            return w, b
        else:
            return (None,None)


    def compute_mean_distance(self, descriptor):
        dist = []
        for my_descriptor in self.descriptors:
            dist.append(np.linalg.norm(my_descriptor - self.center))

        return np.mean(dist)
