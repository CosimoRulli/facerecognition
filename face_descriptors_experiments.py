from vgg  import *
import cv2
import os
from utilities import *
from scipy.spatial.distance import cosine



from scipy.misc import imshow


vgg = load_vgg()

images=[]
path="/home/leonardo/Desktop/dataset/cosi/"
for file in os.listdir(path):
    full_path=path+file
    img=cv2.imread(full_path)
    images.append(img)


descriptors=[]

for image in images:
    b_boxes=extract_faces(image)
    if np.any(b_boxes):
        box= b_boxes[0]
        face = image[box[1]:box[1] + box[3], box[0]:box[0] + box[2]]
        #imshow( face)
        descriptors.append(extract_features(face,vgg))

# for descriptor in descriptors:
#     plt.plot(descriptor)


print(" dist 0 1 ")
distance= np.linalg.norm(descriptors[0]-descriptors[1])
print(distance)
print ("dist 0 2")
distance= np.linalg.norm(descriptors[0]-descriptors[2])
print(distance)

print( "dist 0 3")
distance= np.linalg.norm(descriptors[0]-descriptors[3])
print(distance)
print( "dist 0 4")
distance= np.linalg.norm(descriptors[0]-descriptors[4])
print(distance)

print("dist 1 4 ")
distance= np.linalg.norm(descriptors[1]-descriptors[4])
print(distance)

print("cosine 0 4")
dist = cosine(descriptors[0], descriptors[4])
print(dist)


print("cosine 1 4")
dist = cosine(descriptors[1], descriptors[4])
print(dist)

print("cosine 2 4")
dist = cosine(descriptors[2], descriptors[4])
print(dist)

print("cosine 3 4")
dist = cosine(descriptors[3], descriptors[4])
print(dist)

#plt.show()