"""Performs face alignment and calculates L2 distance between the embeddings of images."""

# MIT License
#
# Copyright (c) 2016 David Sandberg
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from scipy import misc
import tensorflow as tf
import numpy as np
import sys
import os
import copy
import argparse
import facenet.src.facenet as facenet
from Subject import *

# import align.detect_face
import facenet.src.align.detect_face as detect_face
from skimage import exposure
import cv2


def correct_face(face):
    face_gamma = exposure.adjust_gamma(face, 0.2)
    face_gamma = face


    blur5 = cv2.GaussianBlur(face_gamma, (11, 11), 2)  # prova anche 4 che forse è meglio
    blur3 = cv2.GaussianBlur(face_gamma, (5, 5), 1)

    DoGim = blur5 - blur3

    #DoGim[:, :, 0] = DoGim[:, :, 0] / np.power(np.mean(np.power(DoGim[:, :, 0], alpha)), 1 / alpha)
    #DoGim[:, :, 1] = DoGim[:, :, 1] / np.power(np.mean(np.power(DoGim[:, :, 1], alpha)), 1 / alpha)
    #DoGim[:, :, 2] = DoGim[:, :, 2] / np.power(np.mean(np.power(DoGim[:, :, 2], alpha)), 1 / alpha)
    new_indexing = np.argsort([2, 1, 0])
    return DoGim[:,:,new_indexing]



def load_model(path):
    facenet.load_model(path)

def extract_features(faces, sess):
    images_placeholder = tf.get_default_graph().get_tensor_by_name("input:0")
    embeddings = tf.get_default_graph().get_tensor_by_name("embeddings:0")
    phase_train_placeholder = tf.get_default_graph().get_tensor_by_name("phase_train:0")

    # Run forward pass to calculate embeddings

    new_indexing = np.argsort([2,1,0])

    for i in range(len(faces)):
        #faces[i] = exposure.adjust_gamma(faces[i][:,:,new_indexing], 0.5)
        faces[i] = faces[i][:,:,new_indexing]
    feed_dict = {images_placeholder: faces, phase_train_placeholder: False}
    emb = sess.run(embeddings, feed_dict=feed_dict)
    return emb


def facenet_assign_subjects_old(faces , known_subjects, nic_subjects, sess):

    #todo gamma transform
    features=extract_features(faces, sess)
    assigned_subjects=[]
    for i in range(len(faces)):
        best_dist = None
        best_subj = None
        for subject in nic_subjects:
            dist= subject.compute_mean_distance(features[i])
            if not best_dist or best_dist >= dist:
                best_dist=dist
                best_subj=subject
        if not best_subj:
            best_subj = Subject()
            known_subjects.append(best_subj)
        elif not (best_subj.close_enough_eucl(features[i])):
            best_subj = Subject()
            known_subjects.append(best_subj)


        best_subj.update_descriptor(features[i])
        assigned_subjects.append(best_subj)

    return assigned_subjects

def facenet_assign_subjects(faces , known_subjects, nic_subjects, sess):

    #todo gamma transform
    features=extract_features(faces, sess)
    assigned_subjects=[]
    for i in range(len(features)):
        best_dist = []
        candidates = []
        for subject in nic_subjects:
            dist, possible_subj = subject.close_enough_eucl(features[i])
            if possible_subj:
                best_dist.append(dist)
                candidates.append(subject)

        if not candidates:
            best_subj = Subject()
            known_subjects.append(best_subj)
        else:
            index = np.argmax(best_dist)
            best_subj = candidates[index]
            known_subjects.append(best_subj)


        best_subj.update_descriptor(features[i])
        assigned_subjects.append(best_subj)

    return assigned_subjects




def main(args):
    images = load_and_align_data(args.image_files, args.image_size, args.margin, args.gpu_memory_fraction)
    with tf.Graph().as_default():

        with tf.Session() as sess:

            # Load the model
            facenet.load_model(args.model)

            # Get input and output tensors
            images_placeholder = tf.get_default_graph().get_tensor_by_name("input:0")
            embeddings = tf.get_default_graph().get_tensor_by_name("embeddings:0")
            phase_train_placeholder = tf.get_default_graph().get_tensor_by_name("phase_train:0")

            # Run forward pass to calculate embeddings
            feed_dict = {images_placeholder: images, phase_train_placeholder: False}
            emb = sess.run(embeddings, feed_dict=feed_dict)

            nrof_images = len(args.image_files)

            print('Images:')
            for i in range(nrof_images):
                print('%1d: %s' % (i, args.image_files[i]))
            print('')

            # Print distance matrix
            print('Distance matrix')
            print('    ', end='')
            for i in range(nrof_images):
                print('    %1d     ' % i, end='')
            print('')
            for i in range(nrof_images):
                print('%1d  ' % i, end='')
                for j in range(nrof_images):
                    dist = np.sqrt(np.sum(np.square(np.subtract(emb[i, :], emb[j, :]))))
                    print('  %1.4f  ' % dist, end='')
                print('')


def load_and_align_data(image_paths, image_size, margin, gpu_memory_fraction):
    minsize = 20  # minimum size of face
    threshold = [0.6, 0.7, 0.7]  # three steps's threshold
    factor = 0.709  # scale factor

    print('Creating networks and loading parameters')
    with tf.Graph().as_default():
        gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=gpu_memory_fraction)
        sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options, log_device_placement=False))
        with sess.as_default():
            pnet, rnet, onet = detect_face.create_mtcnn(sess, None)

    tmp_image_paths = copy.copy(image_paths)
    img_list = []
    for image in tmp_image_paths:
        img = misc.imread(os.path.expanduser(image), mode='RGB')
        img_size = np.asarray(img.shape)[0:2]
        bounding_boxes, _ = detect_face.detect_face(img, minsize, pnet, rnet, onet, threshold, factor)
        if len(bounding_boxes) < 1:
            image_paths.remove(image)
            print("can't detect face, remove ", image)
            continue
        det = np.squeeze(bounding_boxes[0, 0:4])
        bb = np.zeros(4, dtype=np.int32)
        bb[0] = np.maximum(det[0] - margin / 2, 0)
        bb[1] = np.maximum(det[1] - margin / 2, 0)
        bb[2] = np.minimum(det[2] + margin / 2, img_size[1])
        bb[3] = np.minimum(det[3] + margin / 2, img_size[0])
        cropped = img[bb[1]:bb[3], bb[0]:bb[2], :]
        aligned = misc.imresize(cropped, (image_size, image_size), interp='bilinear')
        prewhitened = facenet.prewhiten(aligned)
        img_list.append(prewhitened)
    images = np.stack(img_list)
    return images


def parse_arguments(argv):
    parser = argparse.ArgumentParser()

    parser.add_argument('model', type=str,
                        help='Could be either a directory containing the meta_file and ckpt_file or a model protobuf (.pb) file')
    parser.add_argument('image_files', type=str, nargs='+', help='Images to compare')
    parser.add_argument('--image_size', type=int,
                        help='Image size (height, width) in pixels.', default=160)
    parser.add_argument('--margin', type=int,
                        help='Margin for the crop around the bounding box (height, width) in pixels.', default=44)
    parser.add_argument('--gpu_memory_fraction', type=float,
                        help='Upper bound on the amount of GPU memory that will be used by the process.', default=1.0)
    return parser.parse_args(argv)


if __name__ == '__main__':
    main(parse_arguments(sys.argv[1:]))
