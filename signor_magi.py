#from Subject import *
from utilities import *
#from vgg import*
import numpy as np
from matplotlib import pyplot as plt
import cv2
from scipy.misc import imsave
import os

def update_stat(face_descriptor, subj,stat):
    count=0
    if subj.stat:
        for i in range(len(face_descriptor)):
            if face_descriptor[i]<= subj.center[i] +subj.dispersion_index[i]:
                count+=1

    stat.append(count/len(face_descriptor))
    print(count/(len(face_descriptor)))

#DoGim[:,:,0]/np.power(np.mean(np.power(DoGim[:,:,0],alpha)),1/alpha)
alpha = 0.1
T = 10
def face_preprocessing():
    dir_path = "/home/leonardo/Pictures/Webcam"
    for img in os.listdir(dir_path):
        path = os.path.join(dir_path, img)

        face = cv2.imread(path)

        new_indexing = np.argsort([2,1,0])

        #resized = cv2.resize(face, (224, 224))
        resized = face
        face_res = correct_face(resized)
        name = dir_path + "prepreocessed" + img

        #imsave("/home/leonardo/Desktop/face_preprocessing/real1.jpg", resized[:,:,new_indexing])
        imsave(name, face_res)



    #face = face[:,:,new_indexing]


    # face_gamma = exposure.adjust_gamma(face, 0.2)
    # #face_gamma = face
    #
    # blur5 = cv2.GaussianBlur(face_gamma, (11, 11), 2) #prova anche 4 che forse è meglio
    # blur5=cv2.GaussianBlur(face_gamma, (23,23), 4 )
    # blur3 = cv2.GaussianBlur(face_gamma, (5, 5), 1)
    #
    # DoGim = blur5 - blur3
    #
    # DoGim[:, :, 0] = DoGim[:, :, 0] / np.power(np.mean(np.power(DoGim[:, :, 0], alpha)), 1 / alpha)
    # DoGim[:, :, 1] = DoGim[:, :, 1] / np.power(np.mean(np.power(DoGim[:, :, 1], alpha)), 1 / alpha)
    # DoGim[:, :, 2] = DoGim[:, :, 2] / np.power(np.mean(np.power(DoGim[:, :, 2], alpha)), 1 / alpha)
    #
    #
    #
    # DoGim[:, :, 0] = DoGim[:, :, 0] / np.power(np.mean(np.power(np.minimum(T,DoGim[:, :, 0]), alpha)), 1 / alpha)
    # DoGim[:, :, 1] = DoGim[:, :, 1] / np.power(np.mean(np.power(np.minimum(T,DoGim[:, :, 1]), alpha)), 1 / alpha)
    # DoGim[:, :, 2] = DoGim[:, :, 2] / np.power(np.mean(np.power(np.minimum(T,DoGim[:, :, 2]), alpha)), 1 / alpha)

    return


if __name__=="__main__":
    face_preprocessing()


def robe():
    vgg = load_vgg()

    cap = cv2.VideoCapture(0)
    subj = Subject()
    statistics=[]



    while(True): #non sarà true ma aspetterà il tocco di un tasto
            #start=time.time()
            ret, frame = cap.read()
            cv2.imshow("frame", frame)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
            #print(ret)

            #frame = cv2.resize(frame, (int(1920/1.5), int(1080/1.2)))

            #2)
            #extracted_faces, b_boxes= extract_faces(frame)
            b_boxes = extract_faces(frame)
            #endbb=time.time()
            if np.array(b_boxes).any():
                box=b_boxes[0]
                face = frame[box[1]:box[1] + box[3], box[0]:box[0] + box[2]]
                face_descriptor = extract_features(face, vgg)
                subj.update_descriptor(face_descriptor)
                update_stat(face_descriptor,subj, statistics)



    plt.plot(statistics)
    plt.show()