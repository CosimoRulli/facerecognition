import cv2
#from vgg_alt import * #TODO quanto è utile
from Subject import Subject
import math
import numpy as np

from vgg import extract_features
from scipy.spatial.distance import cosine

from scipy.misc import imshow
from skimage import exposure
from skimage.filters import rank
from skimage.morphology import disk

def load_subjects():
    return []



#Questo metodo ritorna, data un'immagine, le immagini delle facce e le loro coordinate rispetto all'immagine, espresse come bounding box
def extract_faces(img):
    face_cascade = cv2.CascadeClassifier('haarcascade_xml/haarcascade_frontalface_alt.xml')

    #FIXME Per ora lo convertiamo in grigio
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    b_boxes = face_cascade.detectMultiScale(
        gray,
        scaleFactor=1.1,
        minNeighbors=5,
        minSize=(30, 30),
        #flags=cv2.cv.CV_HAAR_SCALE_IMAGE
    )

    return b_boxes






def split_old_new_lost_faces(current_f_b_boxes, previous_frame, toll):

    new_f_dict={}
    old_f_dict={}

    for new_b_box in current_f_b_boxes:
        new_center=np.array((new_b_box[0] + int(new_b_box[2]/2) , new_b_box[1] + int(new_b_box[3]/2)))
        old=False

        for frame in previous_frame:
            if np.any(frame): #il frame potrebbe essere None a causa dell'initiazlizzazione di previous frame
                for b_box, sbj in frame.items(): #FIXME occhio a .items()
                    center = np.array((b_box[0] + int(b_box[2]/2) , b_box[1] + int(b_box[3]/2)))
                    dist = np.linalg.norm(new_center - center)
                    if (dist < toll):
                        old = True
                        old_f_dict[tuple(new_b_box)] = sbj
        if(not old):
            new_f_dict[tuple(new_b_box)] = None


    previous = set()
    for element in previous_frame:
        if not element == None:
            previous = previous.union(list(element.values()))

    lost_subjects = list(previous - set(list(old_f_dict.values()))) #le facce che c'erano prima ma non ci sono ora
    #lost_subjects = list(set(previous) - set(list(old_f_dict.values())))

    #lost_subjects = list(set([element.values() for element in previous_frame if not element==None]) - set(old_f_dict.values())) #le facce che c'erano prima ma non ci sono ora


    return new_f_dict, old_f_dict, lost_subjects




def split_old_new_lost_faces_old(current_f_b_boxes, pf_dict,toll):

    new_f_dict={}
    old_f_dict={}
    #lost_subjects=[]
    old_b_boxes=pf_dict.keys()

    for (x,y,w,h) in current_f_b_boxes:
        new_center=np.array((x + int(w/2), y + int(h/2)))
        old=False
        for(x1,y1,w1,h1) in old_b_boxes:
            old_center= np.array((x1 + int(w1/2), y1 + int(h1/2)))
            dist=np.linalg.norm(new_center-old_center)
            if(dist<toll):
                old=True
                old_f_dict[(x,y,w,h)]=pf_dict[(x1,y1,w1,h1)]
        if(old==False):
            new_f_dict[(x, y, w, h)]=None



    lost_subjects = list(set(pf_dict.values()) - set(old_f_dict.values())) #le facce che c'erano prima ma non ci sono ora

    return new_f_dict, old_f_dict, lost_subjects


def update_dispersion_metric(face, subject):
    return

alpha = 0.1
T = 10
def correct_face(face):

    new_indexing = np.argsort([2, 1, 0])

    face_CLAHE = exposure.equalize_adapthist(face)
    face_gamma = exposure.adjust_gamma(face_CLAHE, 0.3)

    blur5 = cv2.GaussianBlur(face_gamma, (11, 11), 2)  # prova anche 4 che forse è meglio
    blur3 = cv2.GaussianBlur(face_gamma, (5, 5), 1)

    DoGim = blur5 - blur3

    #DoGim[:, :, 0] = DoGim[:, :, 0] / np.power(np.mean(np.power(DoGim[:, :, 0], alpha)), 1 / alpha)
    #DoGim[:, :, 1] = DoGim[:, :, 1] / np.power(np.mean(np.power(DoGim[:, :, 1], alpha)), 1 / alpha)
    #DoGim[:, :, 2] = DoGim[:, :, 2] / np.power(np.mean(np.power(DoGim[:, :, 2], alpha)), 1 / alpha)
    return DoGim[:,:,new_indexing]



def assign_subject(face , known_subjects,nic_subjects,vgg):

    best_dist = None
    best_subj = None
    new_indexing = np.argsort([2,1,0])

    #face_corrected = exposure.adjust_gamma(face[:,:,new_indexing], 0.5)
    #face_corrected=exposure.adjust_gamma(face, 0.5)
    #face_corrected=exposure.equalize_hist(face)
    #face_corrected=face[:,:,new_indexing]

    #face_corrected = correct_face(face)
    #face_corrected=face[:,:,new_indexing]
    face_descriptor = extract_features( face, vgg)

    for subject in nic_subjects:
        #todo chiamare aggiornamento varianza e centro
        dist = cosine(face_descriptor, subject.center)
        if not best_dist or best_dist >= dist:
            best_dist = dist
            best_subj = subject

    if not best_subj:
        best_subj = Subject()
        known_subjects.append(best_subj)
    elif not (best_subj.close_enough_di(face_descriptor)):
            best_subj = Subject()
            known_subjects.append(best_subj)

    best_subj.update_descriptor(face_descriptor)

    return best_subj


def assign_subject_with_svm(face, known_subject, vgg):

    #todo solo debug
    if not np.array(known_subject).any():
        return assign_subject(face, known_subject, vgg)
    else:
        for subject in known_subject:
            w,b= subject.compute_hyperplane(known_subject)
            if w:
                descriptor= extract_features(face,vgg)

    # for subject in known_subjects:
    #     dist = subject.get_distance(face_descriptor)
    #     if not best_dist or best_dist >= dist:
    #         best_dist = dist
    #         best_subj = subject
    #
    # if not best_subj:
    #     best_subj = Subject()
    #
    # #best_subj.descriptors.append(face_descriptor)
    # best_subj.update_descriptor(face_descriptor)
    #
    # return best_subj


def draw_rect_and_names(dict, frame):
    font = cv2.FONT_HERSHEY_SIMPLEX

    for (x,y,w,h), subject in dict.items():
        cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
        #text=subject.name + " " + subject.surname
        text = "Subject N." + str(subject.id)
        text_size = cv2.getTextSize(text=text, fontFace=font, fontScale=0.8, thickness=2)
        if (y - text_size[0][1] - 5 > 0): #il testo non sborda in alto
            y_pos = y - 5
        else:
            y_pos = y + h + text_size[0][1] + 5

        x_pos=x-10


        cv2.putText(frame, text, (x_pos, y_pos), font, fontScale=1, color=(0, 255, 0), thickness=2)