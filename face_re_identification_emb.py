

'''


'''
from sympy import li

from utilities import *
from vgg import load_vgg, extract_features
import facenet_util
import tensorflow as tf
from scipy import misc
FRAME_MEM = 5

def live_reidentification():

    #TODO IMPLEMENT
    known_subjects = load_subjects() # tramite il file npy immagino

    with tf.Session() as sess:

        facenet_util.load_model("models/20180402-114759")


        previous_frame = [None] * FRAME_MEM #previous frame dictionary, nomenclatura da rivedere al momento dell'implementazione

        #cap = cv2.VideoCapture("/home/leonardo/Downloads/La dottoressa Gastani Frinzi - Il Cosmo sul comò di Aldo Giovanni e Giacomo.mp4")
        cap = cv2.VideoCapture(0)

        while(True): #non sarà true ma aspetterà il tocco di un tasto
            #start=time.time()
            ret, frame = cap.read()

            #print(ret)

            #frame = cv2.resize(frame, (int(1920/1.5), int(1080/1.2)))

            #startbb=time.time()
            #2)
            #extracted_faces, b_boxes= extract_faces(frame)
            b_boxes = extract_faces(frame)
            #endbb=time.time()


            #3)
           # start_split=time.time()
            toll=100
            new_f_dict, old_f_dict, lost_subjects = split_old_new_lost_faces(b_boxes, previous_frame,toll)
           # end_split=time.time()


            #4)
            # for  subject in lost_subjects:#TODO implement -> l'aggiornamento dell'indice di disparsione deve essere implementato
            #     subject.update_dispersion_metric()


            #5)
            #assegno un soggetto ai new entry nel campo della camera e aggiorno i loro descrittori
           # start_ns=time.time()
            nic_subjects = list(set(known_subjects) - set(list(old_f_dict.values())))  # nic=not in cam
            faces = []
            boxes= list(new_f_dict.keys())
            for box in new_f_dict.keys():
                cropped=frame[box[1]:box[1] + box[3], box[0]:box[0] + box[2]]
                aligned = misc.imresize(cropped, (160, 160), interp='bilinear')
                faces.append(aligned)
            if len(faces)!=0:
                assigned= facenet_util.facenet_assign_subjects(faces , known_subjects, nic_subjects, sess) #TODO test

                for i in range(len(assigned)):
                    new_f_dict[boxes[i]] = assigned[i]


            #end_ns=time.time()

            #6)
            #aggiorno i descrittori di old
           # start_old=time.time()
            faces = []
            boxes = old_f_dict.keys()
            for box in old_f_dict.keys():
                cropped = frame[box[1]:box[1] + box[3], box[0]:box[0] + box[2]]
                aligned = misc.imresize(cropped, (160, 160), interp='bilinear')
                faces.append(aligned)

            if len(faces)!=0:
                features=facenet_util.extract_features(faces,sess)

                i=0
                for box, subject in old_f_dict.items():
                    if(not subject.clusterized or np.random.rand(1)> 0.5):
                        face_descriptor = features[i]
                        subject.update_descriptor(face_descriptor)
                    i+=1
          #  end_old=time.time()

            # 7)
            # fonde i dizionari new e old, che raggruppano tutte le facce presenti nel frame
            #start_varie=time.time()
            current_sbj = {**new_f_dict, **old_f_dict}

            previous_frame.insert(0,current_sbj)
            previous_frame.pop()

            draw_rect_and_names(current_sbj, frame)
            cv2.imshow("frame", frame)
           # end_varie=time.time()
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
           # end=time.time()
            # print("Elapsed time for one frame "+ str(end-start))
            # print("Elapsed for bb "+str(endbb-startbb))
            # print("Elapsed for split "+ str(end_split-start_split))
            # print("Elapsed for new subjects "+ str(end_ns-start_ns))
            # print("Elapsed for old subjects "+ str(end_old-start_old))
            # print("Elapsed for varie "+str(end_varie-start_varie))
            #print("\n")


if __name__ == '__main__':
    live_reidentification()