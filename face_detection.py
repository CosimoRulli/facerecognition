import numpy as np
import cv2 as cv
import math


def extract_faces(img):
    face_cascade = cv.CascadeClassifier('/home/cosimo/Documenti/opencvlib/opencv/data/haarcascades/haarcascade_frontalface_alt.xml')
    gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(gray, 1.1, 5)


    return faces

def get_descriptor(face):
    # todo Mettere in vita gli oggetti necessari all'estrazione delle feature tramite vgg e restituire l'array feature
    # vgg_dnn = cv.xfeatures2d_VGG.create()  # https://docs.opencv.org/3.2.0/d6/d00/classcv_1_1xfeatures2d_1_1VGG.html#details
    # detector = cv.xfeatures2d_SURF.create()
    #
    # cv.xfeatures2d_VGG.compute()
    # faces_descr_dict[face]

    descriptor=[]
    return descriptor
if __name__ == '__main__':

    #eye_cascade = cv2.CascadeClassifier('haarcascade_eye_tree_eyeglasses.xml')
    #frame=cv.imread('/home/cosimo/PycharmProject/provaImageProcessing/image.jpg')

    cap=cv.VideoCapture(0)
    while(True):
        ret, frame = cap.read()
        if cv.waitKey(1) & 0xFF == ord('q'):
            break

        b_boxes=extract_faces(frame)

        for (x,y,w,h) in b_boxes:
            #descriptor=get_descriptor(face)
            #faces_descr_dict[face]=descriptor
            face=frame[y:y+h, x:x+w]
            cv.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
            font = cv.FONT_HERSHEY_SIMPLEX
            text_size= cv.getTextSize("tossico",fontFace=font, fontScale=2, thickness=2)
            #print(text_size)
            if(y - text_size[0][1] -5 > 0 ):
                y_pos=y-5
            else:
                y_pos= y + h + text_size[0][1] + 5


            if(text_size[0][0] + x -10< frame.shape[0]):
                x_pos=x-10
            else:
                x_pos= x + w - text_size[0][0]

            cv.putText(frame, "tossico", (x_pos,y_pos), font, 2, (0,255,0), thickness=2)

        cv.imshow("face", frame)
#and text_size[0][0] + x -10< img.shape[0]):



    cv.destroyAllWindows()

