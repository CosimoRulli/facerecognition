

'''

TODO LIST
1) NON LA STESSA FACCIA NELLO STESSO FRAME


1- funzione principale che fa una inizializzazione (si collega alla cam, inizializza il dizonario dei soggetti*, comincia il ciclo while dove si aquisisce il frame
    e chiama le altre funzioni

2f-  estrazione bounding box e immagini faccia da frame

3f- confronto tra frame precedente per individuare l'ingresso o l'uscita di facce dal campo della cam
    ( costruzione di tre dizionari: new, old, lost. NB new avrà i campi value inizialmente a null)

4f-per ogni elemento in lost: aggiorno l'indice di disperisione

5f*-per ogni elemento in new: ricerco tra i soggetti conosciuti ed eventualemente aggiorno i descrittori altrimenti creo un nuovo soggetto
    ( in ogni caso aggiungo il value persona alla key b_box corrispondente).

6f- per ogni elemento in old: aggiorno i  descrittori.

7-a questo punto aggiorno il dizionario bounding box(key) persona (value) tramite le liste old e new ( da farsi nella funzione principale).


FUNZIONI AGGIUNTIVE

1*- persistenza soggetti: prima del while si fa un retrieve dei file .npy dei soggetti salvati e si inizializza il dizionario dei soggetti
    a fine ciclo while verranno salvati i soggetti contenuti nel dizionario.

5*- nel caso in cui in cui i soggetti in new non siano conososciuti ( soggetto non presente nel dizionario -> descrittore non matcha)
    tool di interfaccia che chiede nome e cognome (attributi della classe subject).

'''
from sympy import li

from utilities import *
from vgg import load_vgg, extract_features
import time
FRAME_MEM = 5

def live_reidentification():

    #TODO IMPLEMENT
    known_subjects = load_subjects() # tramite il file npy immagino


    vgg=load_vgg()

    previous_frame = [None] * FRAME_MEM#previous frame dictionary, nomenclatura da rivedere al momento dell'implementazione

    #cap = cv2.VideoCapture("/home/leonardo/Desktop/dataset/video/video_leo_2.mp4")
    cap = cv2.VideoCapture(0)

    while(True): #non sarà true ma aspetterà il tocco di un tasto
        #start=time.time()
        ret, frame = cap.read()

        #print(ret)

        #frame = cv2.resize(frame, (int(1920/1.5), int(1080/1.2)))

        #startbb=time.time()
        #2)
        #extracted_faces, b_boxes= extract_faces(frame)
        b_boxes = extract_faces(frame)
        #endbb=time.time()


        #3)
       # start_split=time.time()
        toll=100
        new_f_dict, old_f_dict, lost_subjects = split_old_new_lost_faces(b_boxes, previous_frame,toll)
       # end_split=time.time()


        #4)
        # for  subject in lost_subjects:#TODO implement -> l'aggiornamento dell'indice di disparsione deve essere implementato
        #     subject.update_dispersion_metric()


        #5)
        #assegno un soggetto ai new entry nel campo della camera e aggiorno i loro descrittori
       # start_ns=time.time()
        nic_subjects = list(set(known_subjects) - set(list(old_f_dict.values())))  # nic=not in cam
        for box in new_f_dict.keys():
            face = frame[box[1]:box[1] + box[3], box[0]:box[0] + box[2]]
            new_f_dict[box] = assign_subject(face , known_subjects, nic_subjects,vgg) #TODO test
        #end_ns=time.time()

        #6)
        #aggiorno i descrittori di old
       # start_old=time.time()
        for box, subject in old_f_dict.items():
            if(not subject.clusterized or np.random.rand(1)> 0.8):
                face= frame[box[1]:box[1] + box[3], box[0]:box[0] + box[2]]
                face_descriptor = extract_features(face,vgg)
                subject.update_descriptor(face_descriptor)
      #  end_old=time.time()

        # 7)
        # fonde i dizionari new e old, che raggruppano tutte le facce presenti nel frame
        #start_varie=time.time()
        current_sbj = {**new_f_dict, **old_f_dict}

        previous_frame.insert(0,current_sbj)
        previous_frame.pop()

        draw_rect_and_names(current_sbj, frame)
        cv2.imshow("frame", frame)
       # end_varie=time.time()
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
       # end=time.time()
        # print("Elapsed time for one frame "+ str(end-start))
        # print("Elapsed for bb "+str(endbb-startbb))
        # print("Elapsed for split "+ str(end_split-start_split))
        # print("Elapsed for new subjects "+ str(end_ns-start_ns))
        # print("Elapsed for old subjects "+ str(end_old-start_old))
        # print("Elapsed for varie "+str(end_varie-start_varie))
        #print("\n")


if __name__ == '__main__':
    live_reidentification()