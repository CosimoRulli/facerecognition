import numpy as np
import cv2
import math

#TODO la struttura del programma nell'estrazione delle facce dall'immagine è pensata tenendo conto del fatto che più facce potrebbero essere
#TODO presenti contemporaneamente, capire se necessario


#Questo metodo ritorna, data un'immagine, le immagini delle facce e le loro coordinate rispetto all'immagine, espresse come bounding box
def extract_faces(img):
    face_cascade = cv2.CascadeClassifier('haarcascade_xml/haarcascade_frontalface_alt.xml')

    #FIXME Per ora lo convertiamo in grigio
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    faces_coord = face_cascade.detectMultiScale(
        gray,
        scaleFactor=1.1,
        minNeighbors=5,
        minSize=(30, 30),
        #flags=cv2.cv.CV_HAAR_SCALE_IMAGE
    )

    extracted_faces = []
    for (x, y, w, h) in faces_coord:
        extracted_faces.append(gray[y:y + h, x:x + w])
    return extracted_faces, faces_coord


#questo metodo divide le facce nuove e vecchie in riferimento a due frame successivi!!
#todo attenzione!!! le new faces non devono essere necessariamente aggiunte alla lista di facce, se una faccia è in quella lista significa
#todo che non c'era (o non era stata rilevata) nel frame analizzato in precedenza


def split_new_and_old_faces_centers(current_frame_centers, previous_frame_centers, toll):

    new_faces=[]
    old_faces=[]

    #todo attenzione al primo frame
    #if (len(previous_frame_centers) == 0):
    #    new_faces=current_frame_centers
    #    return new_faces,old_faces
    for (x,y) in current_frame_centers:
        old = False
        for(x1,y1) in previous_frame_centers:
            distance= math.sqrt( pow(x-x1,2)+pow(y-y1,2) )
            if(distance <=toll):
                old=True
                old_faces.append((x,y))
        if(old==False):
            new_faces.append((x,y))

    return new_faces, old_faces



if __name__== "__main__":
    #cap = cv2.VideoCapture("/home/leonardo/Dropbox/personal/output.mp4")
    cap=cv2.VideoCapture(0)
    savedFrames= []
    i=0
    #cap.set(cv2.CAP_PROP_FPS, 1);#dovrebbe settare il frame rate, NON LO FA

    #lista che mantiene i centri delle facce del frame precedente (precedente in senso di elaborazione)
    previous_frame_centers=[]
    while(True):
        # Capture frame-by-frame
        ret, frame = cap.read()

        #frame = cv2.resize(frame, (int(1920/1.5), int(1080/1.2)))

        centers=[]
        extracted_faces , b_box=extract_faces(frame)
        #center_image_dict=dict(zip(extracted_faces_coord,extracted_faces)) #dizionario che associa i centri delle facce alle relative immagini
        for (x, y, w, h) in b_box:
             x_center= int(x +(w/2))
             y_center=int( y + (h/2))

             centers.append([x_center, y_center])
             #cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
             #cv2.circle(frame, (x_center, y_center), 3, (0, 255, 0), 1 )

        #decommentare per vedere rettangolo attorno alla faccia


        #todo in prima battuta è necessario confrontare i centri dei bounding box appena rilevati con quelli al frame precedente
        #todo se il centro di un b.b. è sufficientemente vicino, non devo aggiungere una nuova faccia, ma aggiornare i descrittori

        #todo leggere descrizione metodo per chiarezza

        toll=500


        new_faces_centers, old_faces_centers=split_new_and_old_faces_centers(centers, previous_frame_centers, toll)

        #Cerchio blu per faccia "nuova" rispetto al frame precedente
        for (x, y) in new_faces_centers:
            cv2.circle(frame, (x,y), 3, (255, 0, 0), 1 )
        #Cerchio verde per faccia "vecchia" rispetto al frame precedente
        for (x, y) in old_faces_centers:
            cv2.circle(frame, (x,y), 3, (0, 255, 0), 1)

        #todo occore risalire dai centri alle facce, che in realtà sono già state estratte.

        # new_faces=[]
        # old_faces=[]
        #
        # for (x,y) in new_faces_centers:
        #      new_faces.append(center_image_dict[(x,y)])
        #      cv2.imshow("newface", center_image_dict[(x,y)])
        # for(x,y) in old_faces_centers:
        #     old_faces_centers.append(center_image_dict[(x,y)])
        #     cv2.imshow("oldface", center_image_dict[x,y])
        #

        cv2.imshow("frame", frame)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
        previous_frame_centers=centers
        i=i+1

    # When everything done, release the

    cap.release()
    cv2.destroyAllWindows()