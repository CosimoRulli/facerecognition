from multiprocessing import Process, Queue, Lock


def produce (queue,id):
    print("putting ", id)
    queue.put(id)

def consume(queue):
    while(True):
        print(queue.get())


if __name__ == '__main__':
    queue=Queue()
    for id in range(0,10):
        Process(target=produce, args=(queue,id)).start()
        Process(target=consume, args=(queue,)).start()

