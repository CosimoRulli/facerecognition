#TODO oltre all'articolo che ci ha mandato ce n'è anche un altro che avevamo visto insieme
#https://medium.com/@franky07724_57962/using-keras-pre-trained-models-for-feature-extraction-in-image-clustering-a142c6cdf5b1


from keras.preprocessing import image
from keras.applications.vgg16 import VGG16
from keras.applications.vgg16 import preprocess_input
import numpy as np
import cv2

def extract_features_alt(img):
    model = VGG16(weights='imagenet', include_top=False)
    model.summary()

    #img_path = '/home/cosimo/PycharmProject/facerecognition/image.jpg'
    #img = image.load_img(img_path, target_size=(224, 224))
    #todo occorre fare il resize dell'immagine passata, oppure passarla 224x224
    resized=cv2.resize(img, (224,224))
    img_data = image.img_to_array(resized)
    img_data = np.expand_dims(img_data, axis=0) # non ho capito questo passaggio
    img_data = preprocess_input(img_data)

    vgg16_feature = model.predict(img_data)

    return vgg16_feature

if __name__=="__main__":
     img = cv2.imread('/home/cosimo/PycharmProject/provaImageProcessing/image_color.jpg')
     features= extract_features_alt(img)
     print(features)